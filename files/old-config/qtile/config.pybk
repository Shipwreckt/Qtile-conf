#     ______     __      __  __                                                 ______   __
#    /      \   /  |    /  |/  |                                               /      \ /  |
#   /$$$$$$  | _$$ |_   $$/ $$ |  ______          _______   ______   _______  /$$$$$$  |$$/   ______
#   $$ |  $$ |/ $$   |  /  |$$ | /      \        /       | /      \ /       \ $$ |_ $$/ /  | /      \
#   $$ |  $$ |$$$$$$/   $$ |$$ |/$$$$$$  |      /$$$$$$$/ /$$$$$$  |$$$$$$$  |$$   |    $$ |/$$$$$$  |
#   $$ |_ $$ |  $$ | __ $$ |$$ |$$    $$ |      $$ |      $$ |  $$ |$$ |  $$ |$$$$/     $$ |$$ |  $$ |
#   $$ / \$$ |  $$ |/  |$$ |$$ |$$$$$$$$/       $$ \_____ $$ \__$$ |$$ |  $$ |$$ |      $$ |$$ \__$$ |
#   $$ $$ $$<   $$  $$/ $$ |$$ |$$       |      $$       |$$    $$/ $$ |  $$ |$$ |      $$ |$$    $$ |
#    $$$$$$  |   $$$$/  $$/ $$/  $$$$$$$/        $$$$$$$/  $$$$$$/  $$/   $$/ $$/       $$/  $$$$$$$ |
#       $$$/                                                                               /  \__$$ |
#                                                                                          $$    $$/
#                                                                                           $$$$$$/


#Colours
white = 'ffffff'
dpurple = '1d141d'
purple = '471e47'

current = '3d1a49'
other = '552467'

#----------------------------------------------------------------------------------#
#                                    Imports                                       #
#              Basically dependancys needed to run some commands                   #
#----------------------------------------------------------------------------------#

from libqtile import bar, layout, widget
from libqtile.config import Click, Drag, Group, Key, Match, Screen
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal
from libqtile import hook
import subprocess

#----------------------------------------------------------------------------------#
#                                     Key binds                                    #
#----------------------------------------------------------------------------------#

mod = "mod4"
terminal = guess_terminal()

keys = [
    Key([mod], "h", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "l", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "k", lazy.layout.up(), desc="Move focus up"),
    Key([mod], "space", lazy.layout.next(), desc="Move window focus to other window"),

    Key([mod, "shift"], "h", lazy.layout.shuffle_left(), desc="Move window to the left"),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right(), desc="Move window to the right"),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down(), desc="Move window down"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(), desc="Move window up"),

    Key([mod, "control"], "h", lazy.layout.grow_left(), desc="Grow window to the left"),
    Key([mod, "control"], "l", lazy.layout.grow_right(), desc="Grow window to the right"),
    Key([mod, "control"], "j", lazy.layout.grow_down(), desc="Grow window down"),
    Key([mod, "control"], "k", lazy.layout.grow_up(), desc="Grow window up"),

    Key([mod], "n", lazy.layout.normalize(), desc="Reset all window sizes"),

    Key([mod, "shift"], "Return", lazy.layout.toggle_split(), desc="Toggle between split and unsplit sides of stack"),
    Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),
    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod], "w", lazy.window.kill(), desc="Kill focused window"),
    Key([mod], "f", lazy.window.toggle_fullscreen(), desc="Toggle fullscreen on the focused window"),
    Key([mod], "t", lazy.window.toggle_floating(), desc="Toggle floating on the focused window"),
    Key([mod, "control"], "r", lazy.reload_config(), desc="Reload the config"),
    Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown Qtile"),

    # Programs shortcuts
    Key([mod], "f", lazy.spawn("librewolf")),
    Key([mod], "d", lazy.spawn("discord")),
    Key([mod], "s", lazy.spawn("steam")),
    Key([mod], "v", lazy.spawn("alacritty -e vim")),
    Key([mod], "p", lazy.spawn("pavucontrol")),
    Key([mod], "n", lazy.spawn("nitrogen")),

    Key([mod], "r", lazy.spawn("rofi -show drun -show-icons")),

    Key([mod, "shift"], "t", lazy.spawn("thunar")),
    Key([mod, "shift"], "p", lazy.spawn("prismlauncher")),
    #Screenshot
    Key([], "Print", lazy.spawn("scrot -s")),
]
#----------------------------------------------------------------------------------#
#                                    Groups                                        #
#----------------------------------------------------------------------------------#


groups = [
    Group("1", label="WWW"),
    Group("2", label="Code"),
    Group("3", label="Chat"),
    Group("4", label="Files"),
    Group("5", label="Games"),
    Group("6", label="Media"),
    Group("7", label="Term"),
    Group("8", label="Sys"),
    Group("9", label="Misc"),
]


for i in groups:
    keys.extend(
        [
            Key(
                [mod],
                i.name,
                lazy.group[i.name].toscreen(),
                desc="Switch to group {}".format(i.name),
            ),

            Key(
                [mod, "shift"],
                i.name,
                lazy.window.togroup(i.name, switch_group=True),
                desc="Switch to & move focused window to group {}".format(i.name),
            ),

        ]
    )

#----------------------------------------------------------------------------------#
#                     Diffrent layouts that are available                          #
#----------------------------------------------------------------------------------#
layouts = [

     layout.Bsp(
        border_focus_stack=["#00ff00", "#00ff00"],
        border_focus= purple,
        border_normal= dpurple,
        border_width=4,
     ),

     layout.Max(),
]

widget_defaults = dict(
    font="bold_sans",
    fontsize=15,
    padding=3,
)

#----------------------------------------------------------------------------------#
#                             Widgets and status bar                               #
#----------------------------------------------------------------------------------#
screens = [ Screen() ]
#----------------------------------------------------------------------------------#
#                             Drag floating layouts.                               #
#----------------------------------------------------------------------------------#


mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(), start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front()),
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: list
follow_mouse_focus = True
bring_front_click = False
floats_kept_above = True
cursor_warp = False
floating_layout = layout.Floating(
    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        *layout.Floating.default_float_rules,
        Match(wm_class="confirmreset"),  # gitk
        Match(wm_class="makebranch"),  # gitk
        Match(wm_class="maketag"),  # gitk
        Match(wm_class="ssh-askpass"),  # ssh-askpass
        Match(title="branchdialog"),  # gitk
        Match(title="pinentry"),  # GPG key password entry
    ]
)
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True


#----------------------------------------------------------------------------------#
#                            If programs want to maxamize                          #
#     this allows things like steam or virtual box to function in full screen      #
#----------------------------------------------------------------------------------#

auto_minimize = True


# When using the Wayland backend, this can be used to configure input devices.
wl_input_rules = None

#----------------------------------------------------------------------------------#
#                  window manager name (will show on neofetch)                     #
#       I suggest just leaving it, since it has something to do with java          #
#----------------------------------------------------------------------------------#


wmname = "LG3D"


#----------------------------------------------------------------------------------#
#                                   Autostart programs                             #
#                     Please be careful and double check if all works              #
#----------------------------------------------------------------------------------#
#                           lauches nitrogen wallpaper                             #
#----------------------------------------------------------------------------------#

@hook.subscribe.startup_once
def autostart():
    subprocess.Popen(["nitrogen", "--restore"])

#----------------------------------------------------------------------------------#
#                                launches picom :D                                 #
#----------------------------------------------------------------------------------#

@hook.subscribe.startup_once
def autostart():
    subprocess.Popen(["picom", "&"], shell=True)

#----------------------------------------------------------------------------------#
#           Runs redshift so I don't get sick after a day of coding                #
#----------------------------------------------------------------------------------#

@hook.subscribe.startup_once
def autostart():
    subprocess.Popen(["redshift"], shell=True)

#----------------------------------------------------------------------------------#
#                      Polybar for the status bar                                  #
#----------------------------------------------------------------------------------#
@hook.subscribe.startup_once
def autostart():
    subprocess.Popen(["polybar"], shell=True)
